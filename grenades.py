'''
Created on Apr 25, 2019

@author: Oak, Glorbonn
Client File
'''
import random
from kafka import KafkaProducer, KafkaConsumer
from _operator import pos

scores =["001", "000"]
topics = []
player_id =0
board = []
banners = ["~ Welcome to Grenades!~\n", 
           "~   You were 'Naded!  ~\n",
           "~   You missed        ~\n",
           "~   That was a hit!   ~\n",]
position = [0,0]
op_position = [0,0]
name =''
opponent =''
ip_list = ["ENTER_BROKER_IP_HERE"] #removed IP for security
score_ready = False
pos_ready = False
victory =["  __     __  ______   ______   ________  ______   _______   __      __",
          "/  |   /  |/      | /      \ /        |/      \ /       \ /  \    /  |",
          "$$ |   $$ |$$$$$$/ /$$$$$$  |$$$$$$$$//$$$$$$  |$$$$$$$  |$$  \  /$$/",
          "$$ |   $$ |  $$ |  $$ |  $$/    $$ |  $$ |  $$ |$$ |__$$ | $$  \/$$/",
          "$$  \ /$$/   $$ |  $$ |         $$ |  $$ |  $$ |$$    $$<   $$  $$/",
          " $$  /$$/    $$ |  $$ |   __    $$ |  $$ |  $$ |$$$$$$$  |   $$$$/",
          "  $$ $$/    _$$ |_ $$ \__/  |   $$ |  $$ \__$$ |$$ |  $$ |    $$ |",
          "   $$$/    / $$   |$$    $$/    $$ |  $$    $$/ $$ |  $$ |    $$ |",
          "    $/     $$$$$$/  $$$$$$/     $$/    $$$$$$/  $$/   $$/     $$/ ",
          "  ______    ______   __    __  ______  ________  __     __  ________  _______ ",
          " /      \  /      \ /  |  /  |/      |/        |/  |   /  |/        |/       \ ",
          "/$$$$$$  |/$$$$$$  |$$ |  $$ |$$$$$$/ $$$$$$$$/ $$ |   $$ |$$$$$$$$/ $$$$$$$  |",
          "$$ |__$$ |$$ |  $$/ $$ |__$$ |  $$ |  $$ |__    $$ |   $$ |$$ |__    $$ |  $$ |",
          "$$    $$ |$$ |      $$    $$ |  $$ |  $$    |   $$  \ /$$/ $$    |   $$ |  $$ |",
          "$$$$$$$$ |$$ |   __ $$$$$$$$ |  $$ |  $$$$$/     $$  /$$/  $$$$$/    $$ |  $$ |",
          "$$ |  $$ |$$ \__/  |$$ |  $$ | _$$ |_ $$ |_____   $$ $$/   $$ |_____ $$ |__$$ |",
          "$$ |  $$ |$$    $$/ $$ |  $$ |/ $$   |$$       |   $$$/    $$       |$$    $$/ ",
          "$$/   $$/  $$$$$$/  $$/   $$/ $$$$$$/ $$$$$$$$/     $/     $$$$$$$$/ $$$$$$$/"]
defeat =["-----------------------------------------------------------------------------",
         " __      __  ______   __    __        _______   ______  ________  _______  ",
         "/  \    /  |/      \ /  |  /  |      /       \ /      |/        |/       \ ",
         "$$  \  /$$//$$$$$$  |$$ |  $$ |      $$$$$$$  |$$$$$$/ $$$$$$$$/ $$$$$$$  |",
         " $$  \/$$/ $$ |  $$ |$$ |  $$ |      $$ |  $$ |  $$ |  $$ |__    $$ |  $$ |",
         "  $$  $$/  $$ |  $$ |$$ |  $$ |      $$ |  $$ |  $$ |  $$    |   $$ |  $$ |",
         "   $$$$/   $$ |  $$ |$$ |  $$ |      $$ |  $$ |  $$ |  $$$$$/    $$ |  $$ |",
         "    $$ |   $$ \__$$ |$$ \__$$ |      $$ |__$$ | _$$ |_ $$ |_____ $$ |__$$ |",
         "    $$ |   $$    $$/ $$    $$/       $$    $$/ / $$   |$$       |$$    $$/ ",
         "    $$/     $$$$$$/   $$$$$$/        $$$$$$$/  $$$$$$/ $$$$$$$$/ $$$$$$$/ ",
         "-----------------------------------------------------------------------------"]

#build producer
producer = KafkaProducer(bootstrap_servers=[ip_list[0]+':9092'])
#build consumers
position_announce = KafkaConsumer('positions', bootstrap_servers=[ip_list[0]+':9092'], 
                         auto_offset_reset='latest')
scores_announce = KafkaConsumer('scores', bootstrap_servers=[ip_list[0]+':9092'], 
                         auto_offset_reset='latest')
grenade_announce = KafkaConsumer('nades', bootstrap_servers=[ip_list[0]+':9092'], 
                         auto_offset_reset='latest')
server = []


def send(topic, MESSAGE):
    """Send a message using Kafka
    """
    #create message
    producer.send(topic, bytes(MESSAGE, encoding = 'utf-8'))
    print("Sent message:"+MESSAGE+" on topic "+topic)
    
def connect_to_game():
    global name, server
    name = input("Who are you?(First initial) ")
    server = KafkaConsumer(name, bootstrap_servers=[ip_list[0]+':9092'], 
                         auto_offset_reset='latest')
    send('connect', name)

    print("Connecting to game Server...")
    i=0
    for message in server:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8')
        global player_id
        player_id = int(data)       
        break

#draw the game board to the console
def draw(banner):
    print("\n~\|/-------------\|/~\n")
    print(banners[banner])
    print("~/|\-------------/|\~")
    print("\n*Scores* "+name+": "+scores[0]+"   "+opponent+": "+ scores[1]+"\n")
    for i in range(0, len(board)): #board height
        curr_row ="|"
        for j in range(0, len(board[i])): #board width
            curr_row = curr_row + board[i][j]
        print(" --- --- --- --- --- --- --- --- --- ---")
        print(curr_row)
    print(" --- --- --- --- --- --- --- --- --- ---")
    #clear old grenades
    for i in range(0, len(board)): #board height
        for j in range(0, len(board[i])): #board width
            if board[i][j] == " X |":
                board[i][j] = "   |"

def player_input():
    action = input("What is your next action?(move, throw, quit) ")
    if action =="move":
        while move() == False:
            continue
            print("Illegal Move")
    elif action == "throw":
        while throw() == False:
            continue
            print("Illegal Throw")
    elif action == "quit":
        return False
    return True

def animate_throw(i, j):
    #3 by 3 block of X for the grenade range
    for x in range(i-1, i+2):
        for y in range(j-1, j+2):
            print(str(x) +" "+str(y))
            #make sure each spot is on the board
            if y >= 0:
                if y <=9:
                    if x >=0:
                        if x<=9:
                            if board[x][y] =="   |":
                                board[x][y] = " X |"
def throw():
    direction = input("Which direction to throw?(R, L, U, D) ")
    send('grenades', name + " " + direction)
    #wait for response
    for message in server:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        if data[0] == 'Ok':
            return True
        else:
            break
    return False

def move():
    print(position)
    direction = input("Which direction to move?(R, L, U, D) ")
    send('move', name + " " + direction)
    
    #wait for response
    for message in server:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        if data[0] == 'Ok':
            return True
        else:
            break
    return False


def end_game(result=3):
    draw(result)
    if int(scores[0]) > 0:
        for line in victory:
            print(line)
    else:
        for line in defeat:
            print(line)

def get_positions():
    print("Getting board positions...")
    global position, op_position, board, name, opponent
    board[position[0]][position[1]] = "   |"
    board[op_position[0]][op_position[1]] = "   |"

    #get initial positions
    for message in position_announce:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()

        if player_id ==0:
            print("Player1 updating positions")
            position[0]=int(data[0])
            position[1]=int(data[1])
            op_position[0]=int(data[2])
            op_position[1]=int(data[3])
        else:
            print("Player2 updating positions")
            position[0]=int(data[2])
            position[1]=int(data[3])
            op_position[0]=int(data[0])
            op_position[1]=int(data[1])

        break
    board[position[0]][position[1]] = " "+name+" |"
    board[op_position[0]][op_position[1]] = " "+opponent+" |"

    
def get_scores():
    print("Getting scores...")
    #get initial positions
    for message in scores_announce:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        global scores, opponent
        if player_id ==0:
            scores[0]= data[1]
            scores[1] = data[3]
            opponent = data[2]
        else:
            scores[0]= data[3]
            scores[1] = data[1]
            opponent = data[0]
        #print("\n*Scores* "+name+": "+scores[0]+"   "+opponent+": "+ scores[1]+"\n")
        break
        
def get_grenades():
    print("Getting grenades...")
    #get initial positions
    for message in grenade_announce:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        if len(data)>0:
            animate_throw(int(data[0]), int(data[1]))
        if len(data)>2:
            animate_throw(int(data[2]), int(data[3]))
        break

#populate an empty board object
def build_board():
    for i in range(0, 10): #board height
        board_row =[]
        for j in range(0, 10): #board width
            board_row.append("   |")
        board.append(board_row)
    get_scores()
    get_positions()

def wait_on_turn():
    print("Waiting on opponent...")
    get_scores()
    get_positions()
    get_grenades()
    
if __name__ == '__main__':
    #set up the game
    #connect to server
    connect_to_game()
    #build the board
    build_board()
    
    #enter game loop
    while True:
        draw(0)
        #ask for input
        if player_input() == False:
            break
        #wait for turn to occur
        wait_on_turn()
        if int(scores[0])<=0:
            end_game(1)
            break
        if int(scores[1])<=0:
            end_game(3)
            break
    #end the game
    producer.close()
    position_announce.close()