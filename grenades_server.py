'''
Created on Apr 25, 2019

@author: Oak, Glorbonn
Server File
'''
import random
from kafka import KafkaProducer, KafkaConsumer
from asyncio.tasks import wait
import time

scores =["999", "999"]
board = []
banners = ["~ Welcome to Grenades!~\n", 
           "~   You were 'Naded!  ~\n",
           "~   You missed        ~\n",
           "~   That was a hit!   ~\n"]
positions = []
player1 =''
player2 =''
ip_list = ["ENTER_BROKER_IP_HERE"] #removed IP for security
nades =[]

#build producer
producer = KafkaProducer(bootstrap_servers=[ip_list[0]+':9092'])
#build consumers
connection = KafkaConsumer('connect', bootstrap_servers=[ip_list[0]+':9092'], auto_offset_reset='latest')
movement = KafkaConsumer('move', bootstrap_servers=[ip_list[0]+':9092'], auto_offset_reset='latest', consumer_timeout_ms=1000)
grenades = KafkaConsumer('grenades', bootstrap_servers=[ip_list[0]+':9092'], auto_offset_reset='latest', consumer_timeout_ms=1000)
#build a player position string
def pos_string():
    position_str = str(positions[0][0])+" "+str(positions[0][1])+ " "+str(positions[1][0])+" "+str(positions[1][1])
    return position_str
#build a player scores string
def score_string():
    scores_str = player1 + " " +str(scores[0])+" "+player2+" "+ str(scores[1])
    return scores_str
def grenades_string():
    nades_str = ''
    for nade in nades:
        for coord in nade:
            nades_str = nades_str+ " "+str(coord)
    return nades_str
    
def draw(banner):
    print("\n~\|/-------------\|/~\n")
    print(banners[banner])
    print("~/|\-------------/|\~")
    print("\n*Scores* "+player1+": "+scores[0]+"   "+player2+": "+ scores[1]+"\n")
    for i in range(0, len(board)): #board height
        curr_row ="|"
        for j in range(0, len(board[i])): #board width
            curr_row = curr_row + board[i][j]
        print(" --- --- --- --- --- --- --- --- --- ---")
        print(curr_row)
    print(" --- --- --- --- --- --- --- --- --- ---")
    #clear old grenades
    for i in range(0, len(board)): #board height
        for j in range(0, len(board[i])): #board width
            if board[i][j] == " X |":
                board[i][j] = "   |"
                
def send(topic, MESSAGE):
    """Send a message using Kafka
    """
    #create message
    producer.send(topic, bytes(MESSAGE,encoding = 'utf-8'))
    print("Sent message:"+MESSAGE+" on topic "+topic)
    #populate an empty board object
    
def build_board():
    for i in range(0, 10): #board height*2
        board_row =[]
        for j in range(0, 10): #board width
            board_row.append("   |")
        board.append(board_row)
    #generate random starting positions
    p1_start = [random.randint(0,9),random.randint(0,9)]
    p2_start = p1_start
    while p2_start == p1_start:
        p2_start = [random.randint(0,9),random.randint(0,9)]
    board[p1_start[0]][p1_start[1]] = " "+player1+" |"
    board[p2_start[0]][p2_start[1]] = " "+player2+" |"
    global positions
    positions.append(p1_start)
    positions.append(p2_start)

def wait_on_connect(players, data):
    global player1, player2
    if players ==2:
        player2 = data
        print("Player 2: "+player2+" connected")
        send(data, '1')
        build_board()
        time.sleep(1)
        send('scores', score_string())
        time.sleep(1)
        send('positions', pos_string())
        return True
    else:
        player1 = data
        send(data, '0')
        print("Player 1: "+player1+" connected")
        return False
        
def check_movement():
    move = False
    global board, positions
    for message in movement:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        name = data[0]
        direction = data[1]
        if name == player1:
            position = positions[0]
        else:
            position = positions[1] 
        print("Recieved "+direction+" move request from "+name)
        print(position)
        if direction == "R":
            if position[1]+1>9 or board[position[0]][position[1]+1]!= "   |":
                print("Illegal Move")
                send(name, 'No')

            else:
                board[position[0]][position[1]] = "   |"
                position[1] +=1
                move = True
        elif direction == "L":
            if position[1]-1<0 or board[position[0]][position[1]-1]!= "   |":
                print("Illegal Move")
                send(name, 'No')

            else:
                board[position[0]][position[1]] = "   |"
                position[1] -=1
                move = True
        elif direction == "U":
            if position[0]-1<0 or board[position[0]-1][position[1]]!= "   |":
                print("Illegal Move")
                send(name, 'No')

            else:
                board[position[0]][position[1]] = "   |"
                position[0] -=1
                move = True
        elif direction == "D":
            if position[0]+1>9 or board[position[0]+1][position[1]]!= "   |":
                print("Illegal Move")
                send(name, 'No')

            else:
                board[position[0]][position[1]] = "   |"
                position[0] +=1
                move = True
    #update the player's position
    if move:
        send(name, 'Ok')
        if name == player1:
            positions[0] = position
        else:
            positions[1] = position 
        board[position[0]][position[1]] = " "+name+" |"
        return 1
    else:
        return 0

def check_throw():
    throw = False
    for message in grenades:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8').split()
        name = data[0]
        direction = data[1]
        if name == player1:
            position = positions[0]
        else:
            position = positions[1] 
        print("Recieved "+direction+" throw request from "+name)
        global nades
        if direction == "R":
            if position[1]+2>9:
                print("Illegal Throw")
                send(name, 'No')
            else:
                nades.append([position[0],position[1]+2])
                throw = True

        elif direction == "L":
            if position[1]-2<0:
                print("Illegal Throw")
                send(name, 'No')
            else:
                nades.append([position[0],position[1]-2])
                throw = True

        elif direction == "U":
            if position[0]-2<0:
                print("Illegal Throw")
                send(name, 'No')
            else:
                nades.append([position[0]-2,position[1]])
                throw = True

        elif direction == "D":
            if position[0]+2>9:
                print("Illegal Throw")
                send(name, 'No')
            else:
                nades.append([position[0]+2,position[1]])
                throw = True
    #update the player's position
    if throw:
        send(name, 'Ok')
        return 1
    else:
        return 0
    
def check_nades():
    global scores, board, nades, game_over
    for nade in nades:
        for x in range(nade[0]-1, nade[0]+2):
            for y in range(nade[0]-1, nade[1]+2):
                #make sure each spot is on the board
                if y >= 0:
                    if y <=9:
                        if x >=0:
                            if x<=9:
                                if board[x][y] ==" "+player1+" |":
                                    print("Player 1 hit")
                                    scores[0] = str(int(scores[0])-333)
                                if board[x][y] ==" "+player2+" |":
                                    print("Player 2 hit")
                                    scores[1] = str(int(scores[1])-333)
        
def run_turn():
    global nades, scores, game_over
    #process turn
    check_nades()
    #send announcements
    send('positions', pos_string())
    time.sleep(1)
    send('scores', score_string())
    time.sleep(1.3)
    send('nades', grenades_string())
    nades =[]
    if int(scores[0])<=0:
        return True
    if int(scores[1])<=0:
        return True
    else:
        return False


if __name__ == '__main__':
    #connect two players
    players = 0
    print("Server is listening:\n")
    for message in connection:
        print ("%s:%d:%d: key=%s value=%s" % (message.topic, message.partition,
                                              message.offset, message.key,
                                              message.value.decode('utf-8')))
        data = message.value.decode('utf-8')
        if message.topic == 'connect':
            players+=1
            if wait_on_connect(players, data):
                break
            
    connection.close()
    turn =1
    #move to turn based loop
    print("Game Start")
    game_over = False
    while True: #for the game
        print("Turn: "+str(turn))
        #for each turn
        ready = 0
        while True:
            move = 0
            throw = 0
            if ready == 2:
                if run_turn():
                    game_over == True
                break
            move = check_movement()
            throw = check_throw()
            ready += move +throw
        turn +=1
            
    
    
    
    